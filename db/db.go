package db

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql" // mysql driver
	"siatka-api/entities/models"
)

func Connect(
	host string,
	port string,
	user string,
	password string,
	dbName string,
) (*gorm.DB, error) {
	connectionString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		user,
		password,
		host,
		port,
		dbName,
	)

	return gorm.Open("mysql", connectionString)
}

func Migrate(dbConn *gorm.DB) error {
	return dbConn.AutoMigrate(
		models.Role{},
		models.User{},
	).Error
}

func Seed(dbConn *gorm.DB) error {
	tx := dbConn.Begin()

	seeders := []func(*gorm.DB) error{
		seedRoles,
		seedUsers,
	}

	for _, seeder := range seeders {
		if err := seeder(tx); err != nil {
			tx.Rollback()
			return err
		}
	}

	return tx.Commit().Error
}
