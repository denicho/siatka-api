package db

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"siatka-api/entities/models"
)

func seedRoles(dbConn *gorm.DB) error {
	roles := []models.Role{
		{
			ID:   1,
			Name: "Kaprodi",
		},
		{
			ID:   2,
			Name: "Sekprodi",
		},
		{
			ID:   3,
			Name: "Dekan",
		},
		{
			ID:   4,
			Name: "Wakil Dekan",
		},
		{
			ID:   5,
			Name: "TU",
		},
		{
			ID:   6,
			Name: "Superuser",
		},
	}

	for _, role := range roles {
		if err := dbConn.FirstOrCreate(&role, models.Role{ID: role.ID}).Error; err != nil {
			return err
		}
	}

	return nil
}

func seedUsers(dbConn *gorm.DB) error {
	users := []models.User{
		{
			ID:       1,
			Username: "superuser",
			Password: "superuser",
			RoleID:   6,
		},
	}

	for _, user := range users {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			return err
		}

		user.Password = string(hashedPassword)

		if err := dbConn.FirstOrCreate(&user, models.User{ID: user.ID}).Error; err != nil {
			return err
		}
	}

	return nil
}
