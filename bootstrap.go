package main

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/jinzhu/gorm"
	httpSwagger "github.com/swaggo/http-swagger"
	"log"
	"net/http"
	"os"
	"siatka-api/db"
	_ "siatka-api/docs" // Swagger Docs
	"siatka-api/handlers"
	"siatka-api/repositories"
	"siatka-api/services"
	"sync"
)

const serviceName = "uajy/siatka"
const serviceVersion = "0.0.1"

var dbSync sync.Once
var dbConn *gorm.DB

var userRepository *repositories.UserRepository

var authService *services.AuthService

func initDB() {
	dbSync.Do(func() {
		conn, err := db.Connect(
			os.Getenv("DB_HOST"),
			os.Getenv("DB_PORT"),
			os.Getenv("DB_USER"),
			os.Getenv("DB_PASSWORD"),
			os.Getenv("DB_DATABASE"),
		)
		if err != nil {
			log.Fatal(err)
		}

		dbConn = conn
	})
}

func initLayers() {
	userRepository = repositories.NewUserRepository(dbConn)

	authService = services.NewAuthService(userRepository)
}

func serveHTTP() {
	router := chi.NewRouter()

	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(render.SetContentType(render.ContentTypeJSON))

	router.Get("/", http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
		payload := map[string]interface{}{
			"name":    serviceName,
			"version": serviceVersion,
		}
		response, _ := json.Marshal(payload)
		writer.Write(response)
	}))

	swaggerURL := fmt.Sprintf("%s/swagger/doc.json", os.Getenv("APP_URL"))
	router.Get("/swagger/*", httpSwagger.Handler(httpSwagger.URL(swaggerURL)))

	router.Mount("/auth", handlers.NewAuthHandler(authService).GetRoutes())

	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}

	host := ""
	if os.Getenv("APP_ENV") == "local" {
		host = "localhost"
	}

	fmt.Printf("App running on port %s\n", port)
	if err := http.ListenAndServe(fmt.Sprintf("%s:%s", host, port), router); err != nil {
		panic(err)
	}
}
