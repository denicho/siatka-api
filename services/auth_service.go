package services

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"os"
	"siatka-api/entities/models"
	"siatka-api/repositories"
	"siatka-api/shared"
	"time"
)

type AuthService struct {
	userRepository *repositories.UserRepository
}

var ErrInvalidCredentials = errors.New("invalid_credentials")

func NewAuthService(userRepository *repositories.UserRepository) *AuthService {
	return &AuthService{
		userRepository: userRepository,
	}
}

func (service *AuthService) Authenticate(ctx context.Context, username, password string) (string, error) {
	user, err := service.userRepository.FindByUsername(ctx, username)
	if err != nil {
		return "", ErrInvalidCredentials
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return "", ErrInvalidCredentials
	}

	accessToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user_id":      user.ID,
		"role_id":      user.RoleID,
		"generated_at": time.Now(),
	})

	tokenString, err := accessToken.SignedString([]byte(os.Getenv("JWT_SIGNING_KEY")))
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (service *AuthService) Whoami(ctx context.Context) (models.User, error) {
	userID := ctx.Value(shared.KeyUserID).(uint)

	return service.userRepository.Find(ctx, userID)
}
