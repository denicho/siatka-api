package middlewares

import (
	"context"
	"github.com/dgrijalva/jwt-go"
	"github.com/mitchellh/mapstructure"
	"net/http"
	"siatka-api/shared"
)

type claimsPayload struct {
	UserID uint `mapstructure:"user_id"`
	RoleID uint `mapstructure:"role_id"`
}

func AuthMiddleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			authHeader := request.Header.Get("Authorization")
			if authHeader == "" {
				http.Error(writer, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			tokenString := authHeader[7:]
			if tokenString == "" {
				http.Error(writer, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			token, _ := jwt.Parse(tokenString, nil)

			var _claimsPayload claimsPayload
			mapstructure.Decode(token.Claims, &_claimsPayload)

			if _claimsPayload.UserID == 0 || _claimsPayload.RoleID == 0 {
				http.Error(writer, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			newRequestCtx := context.WithValue(request.Context(), shared.KeyUserID, _claimsPayload.UserID)
			newRequestCtx = context.WithValue(newRequestCtx, shared.KeyRoleID, _claimsPayload.RoleID)

			next.ServeHTTP(writer, request.WithContext(newRequestCtx))
		})
	}
}
