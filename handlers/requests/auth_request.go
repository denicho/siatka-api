package requests

import (
	"errors"
	"net/http"
)

type AuthRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (req AuthRequest) Bind(request *http.Request) error {
	if req.Username == "" {
		return errors.New("`username` is required")
	}
	if req.Password == "" {
		return errors.New("`password` is required")
	}
	return nil
}
