package responses

import "net/http"

type AuthResponse struct {
	AccessToken string `json:"accessToken"`
}

func (res AuthResponse) Render(writer http.ResponseWriter, request *http.Request) error {
	return nil
}
