package responses

import (
	"net/http"
	"siatka-api/entities/models"
)

type UserResponse struct {
	Data models.User `json:"data"`
}

func (res UserResponse) Render(writer http.ResponseWriter, request *http.Request) error {
	return nil
}
