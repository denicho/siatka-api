package handlers

import (
	"errors"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/jinzhu/gorm"
	"net/http"
	"siatka-api/handlers/middlewares"
	"siatka-api/handlers/requests"
	"siatka-api/handlers/responses"
	"siatka-api/services"
)

type AuthHandler struct {
	authService *services.AuthService
}

func NewAuthHandler(authService *services.AuthService) *AuthHandler {
	return &AuthHandler{
		authService: authService,
	}
}

func (handler *AuthHandler) GetRoutes() chi.Router {
	router := chi.NewRouter()

	router.Post("/", handler.Authenticate)
	router.
		With(middlewares.AuthMiddleware()).
		Get("/whoami", handler.Whoami)

	return router
}

// @Summary Authentication
// @Description Authentication
// @Tags Auth
// @Produce json
// @Accept json
// @Param request body requests.AuthRequest true "Request Body"
// @Success 201 {object} responses.AuthResponse
// @Failure 422 {string} string
// @Failure 500 {string} string
// @Router /auth [post]
func (handler *AuthHandler) Authenticate(writer http.ResponseWriter, request *http.Request) {
	var requestData requests.AuthRequest
	if err := render.Bind(request, &requestData); err != nil {
		http.Error(writer, err.Error(), http.StatusUnprocessableEntity)
		return
	}

	accessToken, err := handler.authService.Authenticate(request.Context(), requestData.Username, requestData.Password)
	if err != nil {
		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	render.Render(writer, request, responses.AuthResponse{
		AccessToken: accessToken,
	})
}

// @Summary Returns current logged user
// @Description Returns current logged user
// @Tags Auth
// @Produce json
// @Accept json
// @Param accessToken header string true "Access Token"
// @Success 200 {object} responses.UserResponse
// @Failure 401 {string} string
// @Failure 500 {string} string
// @Router /auth/whoami [get]
func (handler *AuthHandler) Whoami(writer http.ResponseWriter, request *http.Request) {
	user, err := handler.authService.Whoami(request.Context())
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			http.Error(writer, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}

		http.Error(writer, err.Error(), http.StatusInternalServerError)
		return
	}

	render.Render(writer, request, responses.UserResponse{
		Data: user,
	})
}
