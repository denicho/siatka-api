package repositories

import (
	"context"
	"github.com/jinzhu/gorm"
	"siatka-api/entities/models"
)

type UserRepository struct {
	db *gorm.DB
}

func NewUserRepository(dbConn *gorm.DB) *UserRepository {
	return &UserRepository{
		db: dbConn,
	}
}

func (repository *UserRepository) preload(dbConn *gorm.DB) *gorm.DB {
	return dbConn.Preload("Role")
}

func (repository *UserRepository) FindByUsername(ctx context.Context, username string) (models.User, error) {
	var user models.User

	err := repository.preload(repository.db).
		Where("username = ?", username).
		First(&user).
		Error
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func (repository *UserRepository) Find(ctx context.Context, id uint) (models.User, error) {
	var user models.User

	err := repository.preload(repository.db).
		Where("id = ?", id).
		First(&user).
		Error
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}
