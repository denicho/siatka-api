package models

import "time"

type User struct {
	ID        uint      `json:"id"`
	RoleID    uint      `gorm:"index:users_role_id" json:"-"`
	Username  string    `gorm:"index:users_username" json:"username"`
	Password  string    `json:"-"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	Role      *Role     `json:"role"`
}

type Role struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
