package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"log"
	"os"
	"siatka-api/db"
)

func init() {
	godotenv.Load()
}

func main() {
	initDB()

	command := parseCommand()
	switch command {
	case "serve":
		initLayers()

		serveHTTP()
		break
	case "db-migrate":
		if err := db.Migrate(dbConn); err != nil {
			log.Fatal(err)
		}
		break
	case "db-seed":
		if err := db.Seed(dbConn); err != nil {
			log.Fatal(err)
		}
		break
	default:
		fmt.Printf("Invalid command `%s`\n", command)
	}
}

func parseCommand() string {
	args := os.Args[1:]

	if len(args) > 0 {
		return args[0]
	}
	return ""
}
